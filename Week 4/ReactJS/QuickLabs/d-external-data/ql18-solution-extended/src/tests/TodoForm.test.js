import React from 'react';
import { create, act } from 'react-test-renderer';
import ReactTestUtils from 'react-dom/test-utils';

import TodoForm from '../Components/TodoForm';

jest.mock("../Components/utils/DateCreated", () => {
    return function MockDateCreated() {
        return <span testid="dateCreated">Date Created Component</span>
    }
});

describe(`TodoForm test suite`, () => {

    let submitTodo;
    let testTodo;

    beforeEach(() => {
        submitTodo = jest.fn();
    });

    describe(`DateCreated function and render tests`, () => {

        test(`it should render a DateCreated component a date`, () => {
            testTodo = {
                todoDescription: ``,
                todoDateCreated: ``,
                todoCompleted: false
            };

            const testRenderer = create(<TodoForm submitTodo={submitTodo} todo={testTodo} />);
            const testInstance = testRenderer.root;

            const dateCreated = testInstance.find(
                el => el.type === `span` && el.props.testid === `dateCreated`
            );

            expect(dateCreated).toBeTruthy();
            expect(dateCreated.children).toContain(`Date Created Component`);
        });
    });

    describe(`onChange event tests`, () => {

        test(`it should render the new value in the input when the todoDescription onChange function is activated`, () => {
            testTodo = {
                todoDescription: ``,
                todoDateCreated: ``,
                todoCompleted: false,
                _id: ``
            };

            const testValue = `Test`;

            const testRenderer = create(<TodoForm submitTodo={submitTodo} todo={testTodo} />);
            const testInstance = testRenderer.root;

            const descInput = testInstance.findByProps({ name: "todoDescription" });
            expect(descInput.props.value).toBe(``);

            act(() => descInput.props.onChange({ target: { value: testValue } }));

            expect(descInput.props.value).toBe(testValue);
        });

        test(`it should render the new value in the checkbox when the todoCompleted onChange function is activated`, () => {
            testTodo = {
                todoDescription: ``,
                todoDateCreated: ``,
                todoCompleted: false
            };

            const testRenderer = create(<TodoForm submitTodo={submitTodo} todo={testTodo} />);
            const testInstance = testRenderer.root;
            const completedInput = testInstance.findByProps({ name: "todoCompleted" });

            expect(completedInput.props.checked).toEqual(false);

            act(() => completedInput.props.onChange({ target: { checked: true } }));

            // expect(completedInput.props.checked).toBe(true);
        });
    });

    describe(`Form submission tests`, () => {
        test(`it should call submitTodo with form values`, async () => {
            testTodo = {
                todoDescription: `Test`,
                todoDateCreated: null,
                todoCompleted: false,
                _id: `TestId`
            };
            const testRenderer = create(<TodoForm submitTodo={submitTodo} todo={testTodo} />);
            const testInstance = testRenderer.root;
            const descInput = testInstance.findByProps({ name: "todoDescription" });
            // const descTestValue = `Test`;
            // const compTestValue = false;
            const completedInput = testInstance.findByProps({ name: "todoCompleted" });
            const form = testInstance.findByType('form');

            await act(async () => completedInput.props.onChange({ target: { checked: testTodo.todoCompleted } }));
            await act(async () => descInput.props.onChange({ target: { value: testTodo.todoDescription } }));
            await act(async () => form.props.onSubmit(new Event(`form`)));

            expect(submitTodo).toHaveBeenCalledTimes(1);
            expect(submitTodo).toHaveBeenCalledWith(testTodo);
        });
    });
});