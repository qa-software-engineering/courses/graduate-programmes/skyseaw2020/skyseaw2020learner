import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import 'popper.js';
import 'jquery';
import './Components/css/qa.css';
import axios from 'axios';

import Header from './Components/Header';
import Footer from './Components/Footer';
import AllTodos from './Components/AllTodos';
import AddEditTodo from './Components/AddEditTodo';

const TODOSURL = `http://localhost:4000/todos`;

function App() {
  const [todos, setTodos] = useState([]);
  const [onlineStatus, setOnlineStatus] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      getTodos();
    }, 5000)
  }, []);

  const getTodos = async () => {
    setLoading(true);
    try {
      const res = await axios.get(TODOSURL);
      setTodos(res.data);
      setLoading(false);
      setOnlineStatus(true);
    } catch (e) {
      setTodos(e.message);
      setLoading(false);
      setOnlineStatus(false);
    }
  };

  const postTodo = async todo => {
    try {
      await axios.post(TODOSURL, todo);
      setOnlineStatus(true);
      getTodos();
    } catch (e) {
      setOnlineStatus(false);
    }
  };

  const submitTodo = todo => {
    postTodo(todo);
  }

  return (
    <div className="container">
      <Header />
      <div className="container">
        {!onlineStatus && !loading ? (
          <h3>The data server may be offline, changes will not be
          saved</h3>
        ) : null}
        <AllTodos todos={todos} loading={loading} />
        <AddEditTodo submitTodo={submitTodo} />
      </div>
      <Footer />
    </div>
  );
}

export default App;
