import React from 'react';
import './css/AllTodos.css';

import Todo from './Todo';
import TodoModel from './utils/Todo.model';

const AllTodos = props => {
    let todos = [];
    if (props.todos.length && typeof props.todos[0] !== "string") {
        todos = props.todos.map(currentTodo => {
            const todo = new TodoModel(currentTodo.todoDescription, currentTodo.todoDateCreated, currentTodo.todoCompleted, currentTodo._id);
            return <Todo todo={todo} key={todo._id} />
        });
    } else if (props.loading) {
        todos.push(
            <tr key="loading">
                <td colSpan="3">Please wait - retrieving the todos</td>
            </tr>
        );
    } else {
        todos.push(<tr key="error"><td colSpan="3">There has been an
      error retrieving the todos</td></tr>);
    }

    return (
        <div className="row">
            <h3>Todos List</h3>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>{todos}</tbody>
            </table>
        </div>
    );
};

export default AllTodos;

