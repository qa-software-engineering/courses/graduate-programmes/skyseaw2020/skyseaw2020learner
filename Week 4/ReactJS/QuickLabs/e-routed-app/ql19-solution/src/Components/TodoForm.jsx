import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import DateCreated from './utils/DateCreated';
import generateTodoId from './utils/generateId';

const TodoForm = props => {

    const [todoDescription, setTodoDescription] = useState(``);
    const [todoDateCreated, setTodoDateCreated] = useState(null);
    const [todoCompleted, setTodoCompleted] = useState(false);
    const [todoId, setTodoId] = useState(undefined);

    useEffect(() => {
        const { todoDescription, todoDateCreated, todoCompleted, _id } = props.todo;
        if (Object.getOwnPropertyNames(props.todo).length > 0) {
            setTodoDescription(todoDescription);
            setTodoDateCreated(todoDateCreated);
            setTodoCompleted(todoCompleted);
            setTodoId(_id);
        } else {
            setTodoDescription(``);
            setTodoDateCreated(new Date());
            setTodoCompleted(false);
            setTodoId(generateTodoId());
        }
    }, [props.todo]);

    // Method modified for editing a todo
    const handleSubmit = event => {
        event.preventDefault();
        props.submitTodo({ todoDescription, todoDateCreated, todoCompleted, _id: todoId });
    };

    const disabled = !todoDescription;

    return (
        <div className="container">
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="todoDescription">Description:&nbsp;</label>
                    <input
                        type="text"
                        name="todoDescription"
                        placeholder="Todo description"
                        className="form-control"
                        value={todoDescription}
                        onChange={event => setTodoDescription(event.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="todoDateCreated">Created on:&nbsp;</label>
                    {props.todo.todoDateCreated ? (
                        `${new Date(
                            props.todo.todoDateCreated
                        ).toLocaleDateString()} @ ${new Date(
                            props.todo.todoDateCreated
                        ).toLocaleTimeString()}`
                    ) : (
                            <DateCreated
                                dateCreated={props.todo ? props.todo.todoDateCreated : null}
                                updateDateCreated={dateCreated => setTodoDateCreated(dateCreated)}
                            />
                        )}
                </div>
                {Object.getOwnPropertyNames(props.todo).length > 0 ? (
                    <div className="form-group">
                        <label htmlFor="todoCompleted">Completed: </label>
                        <input
                            type="checkbox"
                            name="todoCompleted"
                            checked={todoCompleted}
                            onChange={e => setTodoCompleted(e.target.checked)}
                        />
                    </div>
                ) : null}
                <div className="form-group">
                    <input type="submit" value="Submit" className={disabled ? `btn btn-danger` : `btn btn-primary`} disabled={disabled} />
                </div>
            </form>
        </div>
    );
};

TodoForm.propTypes = {
    submitTodo: PropTypes.func.isRequired,
    todo: PropTypes.exact({
        todoDescription: PropTypes.string,
        todoDateCreated: PropTypes.string,
        todoCompleted: PropTypes.bool,
        _id: PropTypes.string
    })
};

export default TodoForm;
