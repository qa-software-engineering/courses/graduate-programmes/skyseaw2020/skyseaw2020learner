import React from 'react';
import PropTypes from 'prop-types';
import TodoModel from './utils/Todo.model';

// Desconstruct props to clean code
const Todo = ({ todo, selectTodo }) => {
    const { todoDescription, todoDateCreated, todoCompleted } = todo;
    const dateCreated = new Date(Date.parse(todoDateCreated)).toUTCString();
    const completedClassName = todoCompleted ? `completed` : ``;

    return (
        <tr>
            <td className={completedClassName}>{todoDescription}</td>
            <td className={completedClassName}>{dateCreated}</td>
            <td>
                {/* Render amended to allow editing */}
                {todoCompleted ? (
                    `N/A`
                ) : (
                        <span id="link" onClick={() => selectTodo(todo)}>
                            Edit
                        </span>
                    )}
            </td>
        </tr>
    );
};

Todo.propTypes = {
    todo: PropTypes.instanceOf(TodoModel)
}

export default Todo;
