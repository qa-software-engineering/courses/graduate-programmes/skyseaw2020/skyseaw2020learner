import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './css/AddEditTodo.css';
import TodoForm from './TodoForm';

const AddEditTodo = props => {

    const [todo, setTodo] = useState({});

    useEffect(() => {
        if (
            props.todos &&
            props.todoToUpdate &&
            (Array.isArray(props.todos) &&
                Object.getOwnPropertyNames(props.todoToUpdate).length)
        ) {
            const todoEditing = props.todos.find(
                todoToCheck => todoToCheck._id === props.todoToUpdate._id
            );
            setTodo(todoEditing);
        }
    }, [props.todos, props.todoToUpdate]);

    const submitTodo = submittedTodo => {
        props.submitTodo(submittedTodo);
        setTodo({});
    };

    const action = props.todoToUpdate && Object.getOwnPropertyNames(props.todoToUpdate).length
        ? `Edit`
        : `Add`;

    return (
        <>
            <div className="addEditTodo row">
                <h3>{action} Todo</h3>
                <TodoForm todo={todo} submitTodo={submitTodo} />
            </div>
        </>
    );
}

AddEditTodo.propTypes = {
    submitTodo: PropTypes.func.isRequired,
    todoToUpdate: PropTypes.exact({
        todoDescription: PropTypes.string,
        todoDateCreated: PropTypes.string,
        todoCompleted: PropTypes.bool,
        _id: PropTypes.string
    })
}

export default AddEditTodo;
