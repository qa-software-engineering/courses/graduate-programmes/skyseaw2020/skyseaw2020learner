import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './css/AddEditTodo.css';
// import generateTodoId from './utils/generateId';
import TodoForm from './TodoForm';
// import TodoModel from './utils/Todo.model';


const AddEditTodo = props => {

    // State added to enable editing of Todo
    const [todo, setTodo] = useState({});

    // useEffect added to enable editing of Todo
    useEffect(() => {
        if (
            props.todos &&
            props.todoToUpdate &&
            (Array.isArray(props.todos) &&
                Object.getOwnPropertyNames(props.todoToUpdate).length)
        ) {
            const todoEditing = props.todos.find(
                todoToCheck => todoToCheck._id === props.todoToUpdate._id
            );
            setTodo(todoEditing);
        }
    }, [props.todos, props.todoToUpdate]);

    // Method modified to allow editing of Todo
    const submitTodo = submittedTodo => {
        props.submitTodo(submittedTodo);
        setTodo({});
    };

    // Change the title if editing todo
    const action = props.todoToUpdate && Object.getOwnPropertyNames(props.todoToUpdate).length
        ? `Edit`
        : `Add`;

    return (
        <>
            <div className="addEditTodo row">
                <h3>{action} Todo</h3>
                {/* Form modified to allow editing */}
                <TodoForm todo={todo} submitTodo={submitTodo} />
            </div>
        </>
    );
}

AddEditTodo.propTypes = {
    submitTodo: PropTypes.func.isRequired,
    todoToUpdate: PropTypes.exact({
        todoDescription: PropTypes.string,
        todoDateCreated: PropTypes.string,
        todoCompleted: PropTypes.bool,
        _id: PropTypes.string
    })
}

export default AddEditTodo;
