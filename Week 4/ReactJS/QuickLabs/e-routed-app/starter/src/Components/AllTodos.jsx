import React from 'react';
import PropTypes from 'prop-types';
import './css/AllTodos.css';

import Todo from './Todo';
import TodoModel from './utils/Todo.model';

// props deconstructed for cleaner coding
const AllTodos = ({ todos, selectTodo, loading }) => {
    // todos changed todosToDisplay to avoid destructuring name clash
    let todosToDisplay = [];
    if (todos.length && typeof todos[0] !== "string") {
        // Todo component modified to allow editing of Todos
        todosToDisplay = todos.map(currentTodo => {
            // Destructure currentTodo for cleaner code
            const { todoDescription, todoDateCreated, todoCompleted, _id } = currentTodo;
            const todo = new TodoModel(todoDescription, todoDateCreated, todoCompleted, _id);
            return <Todo
                todo={todo}
                key={todo._id}
                selectTodo={selectTodo}
            />
        });
    } else if (loading) {
        todosToDisplay.push(
            <tr key="loading">
                <td colSpan="3">Please wait - retrieving the todos</td>
            </tr>
        );
    } else {
        todosToDisplay.push(<tr key="error"><td colSpan="3">There has been an
      error retrieving the todos</td></tr>);
    }

    return (
        <div className="row">
            <h3>Todos List</h3>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>{todosToDisplay}</tbody>
            </table>
        </div>
    );
};

// PropTypes added to TypeSafe the component
AllTodos.propTypes = {
    todos: PropTypes.array,
    selectTodo: PropTypes.func,
    loading: PropTypes.bool
};

export default AllTodos;

