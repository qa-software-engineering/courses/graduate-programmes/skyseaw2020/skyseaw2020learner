import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Redirect, useParams } from "react-router-dom";
import './css/AddEditTodo.css';
import TodoForm from './TodoForm';

const AddEditTodo = ({ todos, match, submitTodo: submitTodoFromParent }) => { /*Aliased destructure to avoid name clash*/
    const [todo, setTodo] = useState({});
    const [submitted, setSubmitted] = useState(false);

    const { _id } = useParams();

    useEffect(() => {
        // Edited to use the parameter from the url
        if (todos && _id) {
            const todoEditing = todos.find(
                todoToCheck => todoToCheck._id === _id
            );
            setTodo(todoEditing);
        }
        // Add for correct navigation
        return (() => {
            setTodo({});
            setSubmitted(false);
        });
    }, [todos, _id]);

    const submitTodo = submittedTodo => {
        submitTodoFromParent(submittedTodo);
        setTodo({});
        setSubmitted(true);
    };

    // const action = props.todoToUpdate && Object.getOwnPropertyNames(props.todoToUpdate).length
    const action = todo && match ? `Edit` : `Add`;

    return (
        <>
            {submitted || !todos.length ?
                <Redirect to="/" />
                :
                <div className="addEditTodo row">
                    <h3>{action} Todo</h3>
                    <TodoForm todo={todo} submitTodo={submitTodo} />
                </div>
            }
        </>
    );
}

AddEditTodo.propTypes = {
    submitTodo: PropTypes.func.isRequired
}

export default AddEditTodo;
