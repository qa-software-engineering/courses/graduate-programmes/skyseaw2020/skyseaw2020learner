import React from 'react';
// import PropTypes from 'prop-types';
import './css/AllTodos.css';

import Todo from './Todo';
import TodoModel from './utils/Todo.model';
import { useTodosState } from '../StateManagement/TodosProvider';

const AllTodos = (/*{ todos, loading }*/) => {

    // let todosToDisplay = [];
    // if (todos.length && typeof todos[0] !== "string") {
    //     todosToDisplay = todos.map(currentTodo => {
    //         const { todoDescription, todoDateCreated, todoCompleted, _id } = currentTodo;
    //         const todo = new TodoModel(todoDescription, todoDateCreated, todoCompleted, _id);
    //         return <Todo
    //             todo={todo}
    //             key={todo._id}
    //         />
    //     });
    // } else if (loading) {
    //     todosToDisplay.push(
    //         <tr key="loading">
    //             <td colSpan="3">Please wait - retrieving the todos</td>
    //         </tr>
    //     );
    // } else {
    //     todosToDisplay.push(<tr key="error"><td colSpan="3">There has been an
    //   error retrieving the todos</td></tr>);
    // }

    const { todos } = useTodosState();

    const populateTableBody = () => {

        if (todos?.error) {
            return <tr key="error"><td colSpan="3">{todos.error}</td></tr>
        }

        if (!todos?.length) {
            return <tr key="loading"><td colSpan="3">Data is loading...</td></tr>
        }

        return todos.map(currentTodo => {
            const { todoDescription, todoDateCreated, todoCompleted, _id } = currentTodo;
            const todo = new TodoModel(todoDescription, todoDateCreated, todoCompleted, _id);
            return <Todo
                todo={todo}
                key={todo._id}
            />
        });
    }

    return (
        <div className="row">
            <h3>Todos List</h3>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>{/*todosToDisplay*/ populateTableBody()}</tbody>
            </table>
        </div>
    );
};

// AllTodos.propTypes = {
//     todos: PropTypes.array,
//     selectTodo: PropTypes.func,
//     loading: PropTypes.bool
// };

export default AllTodos;
