import React from 'react';
import { BrowserRouter as Router, Redirect } from 'react-router-dom';
import { create, act } from 'react-test-renderer';
import AddEditTodo from '../Components/AddEditTodo';
import TodoForm from '../Components/TodoForm';
import sampleTodos from '../sampleTodos.json';

jest.mock("../Components/TodoForm", () => {
    return function DummyTodoForm(props) {
        return (
            <form></form>
        );
    }
});

describe(`Tests for AddEditTodo`, () => {
    let testRenderer;
    let testInstance;
    let submitTodo;

    describe(`Add tests`, () => {

        beforeEach(() => {
            submitTodo = jest.fn()
            testRenderer = create(<Router><AddEditTodo submitTodo={submitTodo} todos={sampleTodos} /></Router>);
            testInstance = testRenderer.root;
        });

        afterEach(() => {
            testRenderer = null;
            testInstance = null;
        });

        test(`it should render a div with classNames 'addEditTodo row'`, () => {
            expect(testInstance.findByProps({ className: `addEditTodo row` })).toBeTruthy();

            const h3 = testInstance.findByType(`h3`);
            expect(h3.children).toContain(`Add`);

        });

        test(`it should render a h3 containing Add`, () => {
            const h3 = testInstance.findByType(`h3`);
            expect(h3.children).toContain(`Add`);
        });

        test(`it should render a TodoForm`, () => {
            expect(testInstance.findByType(TodoForm)).toBeTruthy();
        });

        test(`it should call submitTodo when submitTodo is called from props on TodoForm`, () => {
            const testTodo = {
                todoDescription: `Test`,
                todoDateCreated: null,
                todoCompleted: false,
                _id: `TestId`
            };
            const todoForm = testInstance.findByType(TodoForm);

            act(() => {
                todoForm.props.submitTodo(testTodo);
            });

            expect(submitTodo).toHaveBeenCalledTimes(1);
            expect(submitTodo).toHaveBeenCalledWith(testTodo);
        });

        test(`propTypes should be defined`, () => {
            expect(AddEditTodo.propTypes.submitTodo).toBeDefined();
        });


        test(`it should render a div with classNames 'addEditTodo row'`, () => {
            // console.log(testInstance.children);
            // expect(testInstance.findByProps({ className: `addEditTodo row` })).toBeTruthy();
        });
    });

    describe(`Add tests - no todos array`, () => {

        beforeEach(() => {
            submitTodo = jest.fn()
            testRenderer = create(<Router><AddEditTodo submitTodo={submitTodo} todos={[]} /></Router>);
            testInstance = testRenderer.root;
        });

        test(`should render a Redirect`, () => {
            const redirect = testInstance.findByType(Redirect);
            expect(redirect).toBeTruthy();
        });
    });
});
