import React, { createContext, useContext, useEffect, /*useState*/ useReducer } from 'react';
import axios from 'axios';

const TodosStateContext = createContext();
const TodosDispatchContext = createContext();

const baseUrl = `http://localhost:4000/todos`;

const todosReducer = (state, action) => {
    switch (action.type) {
        case `allTodos`: {
            return { ...state, todos: action.payload };
        }
        case `addTodo`: {
            addTodo(action.payload);
            return { todos: [...state.todos, action.payload] };
        }
        case `editTodo`: {
            editTodo(action.payload);
            const todos = [...state.todos];
            const updatedTodos = todos.map(currentTodo => currentTodo._id === action.payload._id ? action.payload : currentTodo);
            return { todos: updatedTodos };
        }
        default:
            throw new Error(`Unhandled action type: ${action.type}`);
    }
}

const addTodo = async newTodo => {
    try {
        const addTodoSuccess = await axios.post(baseUrl, newTodo);
        return addTodoSuccess.data;
    }
    catch (error) {
        return { error: `Todo not added on the server` };
    }
};

const editTodo = async editedTodo => {
    try {
        const editTodoSuccess = await axios.put(`${baseUrl}/${editedTodo._id}`, editedTodo);
        return editTodoSuccess.data;
    }
    catch (error) {
        return { error: `Todo not updated` };
    }
};

export const useTodosState = () => {
    const context = useContext(TodosStateContext);

    if (context === undefined) {
        throw new Error(`useTodosDispatch must be used within a TodosProvider`);
    }

    return context;
}

export const useTodosDispatch = () => {
    const context = useContext(TodosDispatchContext);

    if (context === undefined) {
        throw new Error(`useTodosState must be used within a TodosProvider`);
    }

    return context;
}

const getAllTodos = async () => {
    try {
        const todosData = await axios.get(baseUrl);
        return todosData.data;
    }
    catch (error) {
        return { error: `Data not available from server: ${error.response.status} error` };
    }
}

const TodosProvider = ({ children }) => {
    // const [todos, setTodos] = useState({});
    const [state, dispatch] = useReducer(todosReducer, {});

    useEffect(() => {
        const getTodos = async () => {
            const payload = await getAllTodos();
            // setTodos(payload);
            dispatch({ type: `allTodos`, payload });
        };
        getTodos();
    }, []);

    return (
        <TodosStateContext.Provider value={state}>
            <TodosDispatchContext.Provider value={dispatch}>
                {children}
            </TodosDispatchContext.Provider>
        </TodosStateContext.Provider>
    );
};

export default TodosProvider;