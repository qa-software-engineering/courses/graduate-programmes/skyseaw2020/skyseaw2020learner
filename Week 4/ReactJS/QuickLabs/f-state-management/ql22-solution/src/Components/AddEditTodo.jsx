import React, { useState, useEffect } from 'react';
// import PropTypes from 'prop-types';
import { /*Redirect,*/ useParams } from "react-router-dom";
import './css/AddEditTodo.css';
import TodoForm from './TodoForm';

import { useTodosState } from '../StateManagement/TodosProvider';

const AddEditTodo = (/*{ todos, match, submitTodo: submitTodoFromParent }*/) => {
    const [todo, setTodo] = useState({});
    // const [submitted, setSubmitted] = useState(false);
    const [action, setAction] = useState(`Add`);

    const { todos } = useTodosState();

    const { _id } = useParams();

    useEffect(() => {
        if (todos?.find && _id) {
            setAction(`Edit`);
            const todoEditing = todos.find(
                todoToCheck => todoToCheck._id === _id
            );
            setTodo(todoEditing);
        }

        return (() => {
            setTodo({});
            // setSubmitted(false);
            setAction(`Add`);
        });
    }, [todos, _id]);

    // const submitTodo = submittedTodo => {
    //     submitTodoFromParent(submittedTodo);
    //     setTodo({});
    //     setSubmitted(true);
    // };

    // const action = todo && match ? `Edit` : `Add`;

    return (
        <>
            {/* {submitted || !todos.length ?
                <Redirect to="/" />
                :
                <div className="addEditTodo row">
                    <h3>{action} Todo</h3>
                    <TodoForm todo={todo} submitTodo={submitTodo} />
                </div>
            } */}
            <div className="addEditTodo row">
                <h3>{action} Todo</h3>
                <TodoForm todo={todo} action={`${action.toLowerCase()}Todo`} />
            </div>
        </>
    );
}

// AddEditTodo.propTypes = {
//                 submitTodo: PropTypes.func.isRequired
// }

export default AddEditTodo;
