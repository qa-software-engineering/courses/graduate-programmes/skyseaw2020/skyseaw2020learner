import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import DateCreated from './utils/DateCreated';
import generateTodoId from './utils/generateId';
import { useTodosDispatch } from '../StateManagement/TodosProvider';
import TodoModel from './utils/Todo.model';

const TodoForm = ({ todo, action /*submitTodo*/ }) => {

    const [todoDescription, setTodoDescription] = useState(``);
    const [todoDateCreated, setTodoDateCreated] = useState(null);
    const [todoCompleted, setTodoCompleted] = useState(false);
    // const [todoId, setTodoId] = useState(undefined);

    const [editingTodo, setEditingTodo] = useState(false);
    const [submitted, setSubmitted] = useState(false);

    const dispatch = useTodosDispatch();

    useEffect(() => {
        const { todoDescription, todoDateCreated, todoCompleted, /*_id*/ } = todo;
        if (Object.getOwnPropertyNames(todo).length > 0) {
            setTodoDescription(todoDescription);
            setTodoDateCreated(todoDateCreated);
            setTodoCompleted(todoCompleted);
            // setTodoId(_id);
            setEditingTodo(true);
        }
        // else {
        //     setTodoDescription(``);
        //     setTodoDateCreated(new Date());
        //     setTodoCompleted(false);
        //     setTodoId(generateTodoId());
        // }
        return (() => {
            setTodoDescription(``);
            setTodoDateCreated(new Date());
            setTodoCompleted(false);
            // setTodoId(generateTodoId());
            setEditingTodo(false);
            setSubmitted(false);
        })
    }, [todo]);

    const createTodo = () => {
        const todoId = action === `addTodo` ? generateTodoId() : todo._id;
        return new TodoModel(todoDescription, todoDateCreated, todoCompleted, todoId);
    }

    // Method modified for editing a todo
    const handleSubmit = event => {
        event.preventDefault();
        // submitTodo({ todoDescription, todoDateCreated, todoCompleted, _id: todoId });
        const todo = createTodo();
        dispatch({ type: action, payload: todo });
        setSubmitted(true);
    };

    const redirectAfterSubmit = () => submitted && <Redirect to="/" />

    const disabled = !todoDescription;

    return (
        <>
            {redirectAfterSubmit()}
            <div className="container">
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="todoDescription">Description:&nbsp;</label>
                        <input
                            type="text"
                            name="todoDescription"
                            placeholder="Todo description"
                            className="form-control"
                            value={todoDescription || ``}
                            onChange={event => setTodoDescription(event.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="todoDateCreated">Created on:&nbsp;</label>
                        {/*todo.todoDateCreated*/ editingTodo ? (
                            `${new Date(
                                todo.todoDateCreated
                            ).toLocaleDateString()} @ ${new Date(
                                todo.todoDateCreated
                            ).toLocaleTimeString()}`
                        ) : (
                                <DateCreated
                                    dateCreated={todo ? todo.todoDateCreated : null}
                                    updateDateCreated={dateCreated => setTodoDateCreated(dateCreated)}
                                />
                            )}
                    </div>
                    {Object.getOwnPropertyNames(todo).length > 0 ? (
                        <div className="form-group">
                            <label htmlFor="todoCompleted">Completed: </label>
                            <input
                                type="checkbox"
                                name="todoCompleted"
                                checked={todoCompleted || false}
                                onChange={e => setTodoCompleted(e.target.checked)}
                            />
                        </div>
                    ) : null}
                    <div className="form-group">
                        <input type="submit" value="Submit" className={disabled ? `btn btn-danger` : `btn btn-primary`} disabled={disabled} />
                    </div>
                </form>
            </div>
        </>
    );
};

TodoForm.propTypes = {
    // submitTodo: PropTypes.func.isRequired,
    todo: PropTypes.oneOfType([
        PropTypes.exact({
            todoDescription: PropTypes.string,
            todoDateCreated: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
            todoCompleted: PropTypes.bool,
            _id: PropTypes.string
        }), PropTypes.bool, PropTypes.exact({})
    ]),
    action: PropTypes.string.isRequired
};

export default TodoForm;

