import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import 'popper.js';
import 'jquery';
import './Components/css/qa.css';
import axios from 'axios';

import Header from './Components/Header';
import Footer from './Components/Footer';
import AllTodos from './Components/AllTodos';
import AddEditTodo from './Components/AddEditTodo';

const TODOSURL = `http://localhost:4000/todos`;

function App() {
  const [todos, setTodos] = useState([]);
  const [onlineStatus, setOnlineStatus] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      getTodos();
    }, 5000)
  }, []);

  const submitTodo = todo => {
    let updatedTodos;
    if (typeof todos !== "string") {
      updatedTodos = [...todos];
    }

    const updateIndex = updatedTodos.findIndex(
      storedTodo => storedTodo._id === todo._id
    );

    if (updateIndex === -1) {
      Array.isArray(updatedTodos)
        ? updatedTodos.push(todo)
        : (updatedTodos = [todo]);
      postTodo(todo);
    } else {
      updatedTodos[updateIndex] = todo;
      updateTodo(todo);
    }

    setTodos(updatedTodos);
  };

  const getTodos = async () => {
    setLoading(true);
    try {
      const res = await axios.get(TODOSURL);
      setTodos(res.data);
      setLoading(false);
      setOnlineStatus(true);
    } catch (e) {
      setTodos(e.message);
      setLoading(false);
      setOnlineStatus(false);
    }
  };

  const postTodo = async todo => {
    try {
      await axios.post(TODOSURL, todo);
      setOnlineStatus(true);
      await getTodos();
    } catch (e) {
      setOnlineStatus(false);
    }
  };

  const updateTodo = async todo => {
    try {
      await axios.put(`${TODOSURL}/${todo._id}`, todo);
      setOnlineStatus(true);
      await getTodos();
    } catch (e) {
      setOnlineStatus(false);
    }
  };

  return (
    <Router>
      <div className="container">
        <Header />
        <div className="container">
          {!onlineStatus && !loading ? (
            <h3>The data server may be offline, changes will not be
            saved</h3>
          ) : null}
          <Switch>
            <Route exact path="/">
              <AllTodos todos={todos} loading={loading} />
            </Route>
            <Route path="/add">
              <AddEditTodo submitTodo={submitTodo} todos={todos} />
            </Route>
            <Route path="/edit/:_id">
              <AddEditTodo submitTodo={submitTodo} todos={todos} />
            </Route>
          </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
