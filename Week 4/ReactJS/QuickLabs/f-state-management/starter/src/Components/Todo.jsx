import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import TodoModel from './utils/Todo.model';

// selectTodo removed from props as no longer needed
const Todo = ({ todo, /* selectTodo */ }) => {
    const { todoDescription, todoDateCreated, todoCompleted } = todo;
    const dateCreated = new Date(Date.parse(todoDateCreated)).toUTCString();
    const completedClassName = todoCompleted ? `completed` : ``;

    return (
        <tr>
            <td className={completedClassName}>{todoDescription}</td>
            <td className={completedClassName}>{dateCreated}</td>
            <td>
                {todo.todoCompleted ? (
                    `N/A`
                ) : (
                        <Link className="link" to={`/edit/${todo._id}`}>
                            Edit
                        </Link>
                    )}
            </td>
        </tr>
    );
};

Todo.propTypes = {
    todo: PropTypes.instanceOf(TodoModel)
}

export default Todo;

