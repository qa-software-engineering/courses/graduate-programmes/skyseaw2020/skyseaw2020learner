package qa.emp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import qa.emp.domain.Employee;
import qa.emp.repo.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDaoSpringData implements EmployeeDao {
    @Autowired
    private EmployeeRepository repo;

    public List<Employee> getEmployees() {
        List<Employee> employees = new ArrayList<Employee>();
        for (Employee e : repo.findAll()) {
            employees.add(e);
        }
        return employees;
    }

    public boolean addEmployee(Employee e) {
        int id = (int) (System.currentTimeMillis() % 100000);
        e.setId(id);
        repo.save(e);
        return true;
    }

    public boolean removeEmployee(Employee e) {
        e = findEmployee(e.getFirstname(), e.getLastname());  // fetch the id
        if (e==null) return false;
        repo.delete(e);
        return true;
    }

    public Employee findEmployee(String firstname, String lastname) {
        return repo.findByFirstnameAndLastname(firstname,lastname);
    }

    public Employee findEmployee(int id){
            return repo.findById(id).get();
    }

    public void close() {
    }
}
