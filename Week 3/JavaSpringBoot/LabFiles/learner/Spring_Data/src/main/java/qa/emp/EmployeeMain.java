package qa.emp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import qa.emp.app.EmployeeController;

@SpringBootApplication
public class EmployeeMain implements CommandLineRunner {
	@Autowired
	EmployeeController controller;

	public static void main(String[] args) {
		SpringApplication.run(EmployeeMain.class, args);
	}

	public void run(String... args) throws Exception {
	}
}
