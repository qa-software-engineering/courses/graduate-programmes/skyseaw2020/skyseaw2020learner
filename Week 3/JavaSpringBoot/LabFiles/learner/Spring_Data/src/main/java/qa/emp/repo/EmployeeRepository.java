package qa.emp.repo;

import org.springframework.data.repository.CrudRepository;
import qa.emp.domain.Employee;

public interface EmployeeRepository extends CrudRepository<Employee,Integer> {
    public Employee findByFirstnameAndLastname(String firstname, String lastname);
}
