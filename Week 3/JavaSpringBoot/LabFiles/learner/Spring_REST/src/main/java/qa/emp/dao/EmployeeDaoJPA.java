package qa.emp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import qa.emp.domain.Employee;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EmployeeDaoJPA implements EmployeeDao {

    @PersistenceContext
    private EntityManager em;

    public EmployeeDaoJPA() {
    }

    public List<Employee> getEmployees() {
        return em.createQuery("from Employee").getResultList();
    }

    @Transactional
    public boolean addEmployee(Employee e) {
        int id = (int) (System.currentTimeMillis() % 10000);
        em.persist(e);
        return true;
    }

    @Transactional
    public boolean removeEmployee(Employee e) {
        if (e==null) return false;
        try {
            e = em.getReference(Employee.class, e.getId());
            em.remove(e);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
        return true;
    }

    public void close() {
        em.close();
    }

    public Employee findEmployee(String firstname, String lastname) {
        try {
            Employee e = (Employee) em.createQuery("from Employee e where e.firstname=:firstname and e.lastname=:lastname")
                    .setParameter("firstname", firstname)
                    .setParameter("lastname", lastname)
                    .getSingleResult();
            return e;
        } catch (Exception ex) {
            return null;
        }

    }
}
