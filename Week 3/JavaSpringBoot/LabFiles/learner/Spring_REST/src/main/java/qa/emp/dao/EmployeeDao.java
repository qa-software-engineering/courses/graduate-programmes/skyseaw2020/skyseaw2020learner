package qa.emp.dao;

import qa.emp.domain.Employee;

import java.util.List;

public interface EmployeeDao {
    public List<Employee> getEmployees();
    public boolean addEmployee(Employee e);
    public boolean removeEmployee(Employee e);
    public Employee findEmployee(String firstname, String lastname);
    public void close();
}
