package qa.emp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import qa.emp.controller.EmployeeController;
import qa.emp.controller.EmployeeControllerImpl;
import qa.emp.dao.EmployeeDao;
import qa.emp.dao.EmployeeDaoJPA;

@Configuration
public class Config {
    @Bean
    public EmployeeDao employeeDao() {
        return new EmployeeDaoJPA();
    }

    @Bean
    public EmployeeController employeeController() {
        return new EmployeeControllerImpl();
    }
}
