package qa.emp.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import qa.emp.dao.EmployeeDao;
import qa.emp.domain.Employee;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeDao dao;

    @GetMapping
    public ResponseEntity<List<Employee>> getAllEmployees() {
        return new ResponseEntity<List<Employee>>(dao.getEmployees(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable int id) {
        return new ResponseEntity<Employee>(dao.findEmployee(id), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Employee> insert(@RequestBody Employee emp, UriComponentsBuilder builder) { // need some kind of parameter and response?
        HttpHeaders headers = new HttpHeaders();
        boolean success = dao.addEmployee(emp);
        int newId = emp.getId();
        if (!success) { // didn't work - probably a dup key
            return new ResponseEntity<Employee>(emp, headers, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<Employee>(emp, headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable int id) {
        Employee emp = dao.findEmployee(id);
        if (emp == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        dao.removeEmployee(emp);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
