package qa.emp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qa.emp.dao.EmployeeDao;
import qa.emp.domain.Employee;

import java.util.List;

@Service
public class EmployeeControllerImpl implements EmployeeController {
    @Autowired
    private EmployeeDao dao;

    public EmployeeControllerImpl() {
    }

    public boolean createEmployee(String firstname, String lastname, int age) {
        int id = (int) (System.currentTimeMillis() % 10000);
        Employee e = new Employee(id,firstname,lastname,age);
        return dao.addEmployee(e);
    }

    public boolean deleteEmployee(String firstname, String lastname) {
        Employee e = dao.findEmployee(firstname, lastname);
        if (e==null) return false;
        return dao.removeEmployee(e);
    }

    public String getAllEmployees() {
        StringBuilder sb = new StringBuilder();
        List<Employee> allEmployees = dao.getEmployees();
        for (Employee e : allEmployees) {
            sb.append(e.getFirstname() + "\t" + e.getLastname() + "\t" + e.getAge() + "\n");
        }
        return sb.toString();
    }

    public void close() {
       dao.close();
    }
}
