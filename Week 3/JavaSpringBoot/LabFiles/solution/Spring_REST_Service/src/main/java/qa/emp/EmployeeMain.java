package qa.emp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import qa.emp.app.EmployeeController;
import qa.emp.gui.EmployeeFrame;

@SpringBootApplication
public class EmployeeMain implements CommandLineRunner {
	@Autowired
	EmployeeController controller;

	public static void main(String[] args) {
		SpringApplication.run(EmployeeMain.class, args);
		//SpringApplicationBuilder builder = new SpringApplicationBuilder(EmployeeMain.class);
		//builder.headless(false);
		//builder.run(args);
	}

	public void run(String... args) throws Exception {
		//EmployeeFrame frame = new EmployeeFrame(controller);
		//frame.launch();
	}
}
