package qa.emp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import qa.emp.controller.EmployeeController;
import qa.emp.gui.EmployeeFrame;

public class EmployeeMain {

	public static void main(String[] args) {
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext("qa.emp.config");
		EmployeeController controller = ctx.getBean(EmployeeController.class);
		EmployeeFrame frame = new EmployeeFrame(controller);
		frame.launch();
		
	}

}
