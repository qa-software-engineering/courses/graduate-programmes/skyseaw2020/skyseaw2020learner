package qa.emp.dao;

import qa.emp.domain.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class EmployeeDaoJPA implements EmployeeDao {
    private EntityManager em;

    public EmployeeDaoJPA() {
        EntityManagerFactory fac = Persistence.createEntityManagerFactory("MyPersistenceUnit");
        em = fac.createEntityManager();
    }

    public List<Employee> getEmployees() {
        return em.createQuery("from Employee").getResultList();
    }

    public boolean addEmployee(Employee e) {
        int id = (int) (System.currentTimeMillis() % 10000);
        em.getTransaction().begin();
        em.persist(e);
        em.getTransaction().commit();
        return true;
    }

    public boolean removeEmployee(Employee e) {
        if (e==null) return false;
        try {
            em.getTransaction().begin();
            em.remove(e);
            em.getTransaction().commit();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public void close() {
        em.close();
    }

    public Employee findEmployee(String firstname, String lastname) {
        try {
            Employee e = (Employee) em.createQuery("from Employee e where e.firstname=:firstname and e.lastname=:lastname")
                    .setParameter("firstname", firstname)
                    .setParameter("lastname", lastname)
                    .getSingleResult();
            return e;
        } catch (Exception ex) {
            return null;
        }

    }
}
