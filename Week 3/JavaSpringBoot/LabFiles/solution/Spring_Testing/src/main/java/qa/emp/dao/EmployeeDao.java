package qa.emp.dao;

import qa.emp.domain.Employee;

import java.util.List;

public interface EmployeeDao {
    public Employee findById(int id);
    public List<Employee> findAll();
    public int insert(Employee emp);
    public boolean delete(Employee emp);
}
