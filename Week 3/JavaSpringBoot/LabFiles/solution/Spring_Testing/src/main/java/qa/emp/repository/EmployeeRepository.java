package qa.emp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import qa.emp.domain.Employee;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee,Integer> {
    public List<Employee> findByAgeBetween(int lower, int upper);
}
