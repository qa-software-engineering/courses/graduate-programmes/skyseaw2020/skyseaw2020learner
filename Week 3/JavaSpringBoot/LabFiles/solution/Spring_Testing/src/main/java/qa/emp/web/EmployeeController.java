package qa.emp.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import qa.emp.dao.EmployeeDao;
import qa.emp.domain.Employee;

import java.util.ArrayList;
import java.util.List;

@RestController
public class EmployeeController {
    private Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    @Autowired
    private EmployeeDao dao;

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable("id") int id) {
        logger.debug("Trying to find " + id);
        return dao.findById(id);
    }

    @GetMapping
    public List<Employee> getAllEmployees() {
        return dao.findAll();
    }

    @PostMapping
    public ResponseEntity<Employee> insert(@RequestBody Employee emp) {
        logger.debug("Trying to insert " + emp);
        int newId = dao.insert(emp);
        if (newId < 0) { // didn't work - probably a dup key
            logger.debug("Failed to insert " + emp);
            HttpHeaders headers = new HttpHeaders();
            return new ResponseEntity<Employee>(emp, headers, HttpStatus.CONFLICT);
        }

        HttpHeaders headers = new HttpHeaders();
        emp.setId(newId);
        return new ResponseEntity<Employee>(emp, headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable int id) {
        Employee emp = dao.findById(id);
        logger.debug("Trying to delete " + emp);
        if (emp == null) {
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        dao.delete(emp);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
