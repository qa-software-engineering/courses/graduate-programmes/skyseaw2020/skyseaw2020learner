package qa.emp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qa.emp.repository.EmployeeRepository;
import qa.emp.domain.Employee;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeDaoSpringData implements EmployeeDao {
    @Autowired private EmployeeRepository employeeRepository;

    @Override
    public Employee findById(int id) {
        return employeeRepository.findById(id).orElse(null);
    }

    @Override
    public List<Employee> findAll() {
        List<Employee> employeeList = new ArrayList<Employee>();
        for (Employee emp : employeeRepository.findAll()) {
            employeeList.add(emp);
        }
        return employeeList;
    }

    @Override
    public int insert(Employee emp) {
        if (employeeRepository.existsById(emp.getId())) return -1;
        emp = employeeRepository.save(emp);
        return emp.getId();
    }

    @Override
    public boolean delete(Employee emp) {
        if (!employeeRepository.existsById(emp.getId())) return false;
        employeeRepository.delete(emp);
        return true;
    }
}
