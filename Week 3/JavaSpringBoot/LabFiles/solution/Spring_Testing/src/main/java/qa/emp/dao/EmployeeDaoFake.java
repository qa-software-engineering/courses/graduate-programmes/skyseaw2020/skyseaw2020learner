package qa.emp.dao;

import org.springframework.stereotype.Service;
import qa.emp.domain.Employee;

import java.util.ArrayList;
import java.util.List;

//@Service
public class EmployeeDaoFake implements EmployeeDao {
    private List<Employee> allEmployees;

    public EmployeeDaoFake() {
        this.allEmployees = new ArrayList<Employee>();
        allEmployees.add(new Employee( "Lewis", 47, 50000.0));
        allEmployees.add(new Employee("Fred", 24, 30000.0));
        allEmployees.add(new Employee("Sue", 31, 40000.0));
    }

    public Employee findById(int id) {
        for (Employee emp : allEmployees) {
            if (emp.getId()==id) return emp;
        }
        return null;
    }

    public List<Employee> findAll() {
        return new ArrayList<Employee>(allEmployees);
    }

    public int insert(Employee emp) {
        allEmployees.add(emp);
        return emp.getId();
    }

    public boolean delete(Employee emp) {
        return allEmployees.remove(emp);
    }
}
