package qa.emp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import qa.emp.dao.EmployeeDao;
import qa.emp.domain.Employee;

@SpringBootApplication
public class EmployeeApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(EmployeeApplication.class, args);
    }

    @Autowired
    private EmployeeDao dao;
    @Override
    public void run(String... args) throws Exception {
        dao.insert(new Employee("Jim", 67, 100000.0));
        dao.insert(new Employee("Boris", 55, 60000.0));
    }
}
