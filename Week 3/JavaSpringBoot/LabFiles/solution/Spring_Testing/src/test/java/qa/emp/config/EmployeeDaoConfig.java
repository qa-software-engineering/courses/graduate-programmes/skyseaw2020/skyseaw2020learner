package qa.emp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import qa.emp.dao.EmployeeDao;

import static org.mockito.Mockito.mock;

@Configuration
public class EmployeeDaoConfig {
    @Bean
    public EmployeeDao dao() {
        return mock(EmployeeDao.class);
    }
}
