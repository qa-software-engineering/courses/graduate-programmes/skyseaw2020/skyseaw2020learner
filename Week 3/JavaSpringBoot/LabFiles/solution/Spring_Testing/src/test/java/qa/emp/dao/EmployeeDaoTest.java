package qa.emp.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import qa.emp.domain.Employee;
import qa.emp.repository.EmployeeRepository;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.hamcrest.number.OrderingComparison.*;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration()
//@EnableJpaRepositories(basePackages = "qa.emp.repository")
//@EntityScan(basePackages = "qa.emp.domain")
//@DataJpaTest
//@TestPropertySource("classpath:application.properties")
//@SpringBootTest
public class EmployeeDaoTest {

   // @Autowired
    private EmployeeDao dao;

    //@Test
    public void testInsertHappened() {
        dao.insert(new Employee("Lewis", 47, 30000.0));
        assertThat(dao.findAll().size(), is(1));
        int id = dao.insert(new Employee("Fred", 35, 30000.0));
        assertThat(id, greaterThanOrEqualTo(0));
        assertThat(dao.findAll().size(), is(2));
    }

}