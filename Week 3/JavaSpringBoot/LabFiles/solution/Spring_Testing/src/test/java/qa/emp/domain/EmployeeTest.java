package qa.emp.domain;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.number.OrderingComparison.;



public class EmployeeTest {

    @Test
    public void testEmployeeInitialSalaryZero() {
        Employee emp = new Employee();
        assertThat(emp.getSalary(), closeTo(0.0, 0.01));
    }

    @Test
    public void testEmployeeInitialSalarySetByConstructor() {
        Employee emp = new Employee("Fred", 35, 20000.0);
        assertThat(emp.getSalary(), closeTo(20000.0, 0.01));
    }

    @Test
    public void testEmployeePayRise20000By10pc() {
        Employee emp = new Employee("Fred", 35, 20000.0);
        emp.payrise(10.0);
        assertThat(emp.getSalary(), closeTo(22000.0, 0.01));
    }
}