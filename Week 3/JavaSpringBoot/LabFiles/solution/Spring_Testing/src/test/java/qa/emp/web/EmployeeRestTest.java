package qa.emp.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import qa.emp.dao.EmployeeDao;
import qa.emp.domain.Employee;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeRestTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private EmployeeDao dao;

    @Test
    public void testFoundEmployee1() throws Exception {
        dao.insert(new Employee("Fred", 35, 30000.0));
        //mockMvc.perform(get("/1")).andReturn().getResponse().getContentAsString()
        mockMvc.perform(get("/1")).andExpect(status().isOk());
    }
}
