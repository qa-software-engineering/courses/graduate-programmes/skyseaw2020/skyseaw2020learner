package qa.emp.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import qa.emp.config.EmployeeDaoConfig;
import qa.emp.dao.EmployeeDao;
import qa.emp.domain.Employee;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.core.Is.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = EmployeeDaoConfig.class)
@SpringBootTest(classes = EmployeeController.class)
public class EmployeeControllerTest {
    @Autowired
    private EmployeeController rest;
    @Autowired
    private EmployeeDao dao;

    @Test
    public void insertEmployeeFails() {
        Employee emp = new Employee("Fred", 35, 30000.0);
        when(dao.insert(emp)).thenReturn(-1);  // pretend it fails in the DAO
        ResponseEntity<Employee> response = rest.insert(emp);
        assertThat(response.getStatusCodeValue(), is(HttpStatus.CONFLICT.value()));
    }
}