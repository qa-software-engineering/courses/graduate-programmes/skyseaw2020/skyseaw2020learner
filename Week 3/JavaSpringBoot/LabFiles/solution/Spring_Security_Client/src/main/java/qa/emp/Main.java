package qa.emp;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilderFactory;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

public class Main {
    private static RestTemplate restTemplate;

    public static void main(String[] args) throws Exception {
        CredentialsProvider provider = new BasicCredentialsProvider();

        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("fred", "fredpw");
        provider.setCredentials(AuthScope.ANY, credentials);
        HttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory= new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setHttpClient(httpClient);
        restTemplate = new RestTemplate(); //new RestTemplate(clientHttpRequestFactory);

        System.out.println("----- Initial list -----");
        printAndgetAllEmployees();

        Employee emp = new Employee(0, "Lewis","Pirnie", 47);

        RequestEntity<Employee> req1 = new RequestEntity<Employee>(emp, HttpMethod.POST, new URI("http://localhost:8080/"));
        ResponseEntity<Void> resp1 = restTemplate.exchange(req1, new ParameterizedTypeReference<Void>(){});

        System.out.println("----- Updated list -----");
        List<Employee> allEmployeesAfter = printAndgetAllEmployees();

        for (Employee e : allEmployeesAfter) {
            if (e.getFirstname().equals("Donald") && e.getLastname().equals("Duck")) {

                UriBuilderFactory fac = new DefaultUriBuilderFactory();
                URI uriToDelete = fac.uriString("http://localhost:8080/{id}").build(e.getId());

                RequestEntity<Void> reqToDelete = RequestEntity.delete(uriToDelete).build();
                ResponseEntity<Void> respFromDelete = restTemplate.exchange(reqToDelete, Void.class);
                System.out.println("Delete status: " + respFromDelete.getStatusCode());

            }
        }

        System.out.println("----- Repaired list -----");
        printAndgetAllEmployees();



    }

    private static List<Employee> printAndgetAllEmployees() {
        ResponseEntity<Employee[]> allResponse = restTemplate.exchange("http://localhost:8080/",
                        HttpMethod.GET, null, new ParameterizedTypeReference<Employee[]>(){});
        Employee[] employees = allResponse.getBody();
        for (Employee employee : employees) {
            System.out.println(employee);
        }
        return Arrays.asList(employees);
    }
}
